= Demo Songbook for Zach's Songbook builder

Here is an example project for creating your own songbook using [Zach's songbook builder](https://zachcapalbo.com/projects/songbook.html)

All you have to do to create your own songbook is [Fork this repository](https://gitlab.com/zach-geek/demo-songbook/-/forks/new). Then customize the songbook.yml file, and start adding your own songs to the `Songs` directory. You can do this all through the GitLab WebIDE interface without installing anything! You should also change this Readme.md file, since it will show up as the prefact to your songbook.

Then anytime you make any changes, just use the Web IDE to commit them to the main brainch, and then GitLab should automatically build the songbook. Look in the job build artifacts for the pdfs. Print them out and have fun singing!